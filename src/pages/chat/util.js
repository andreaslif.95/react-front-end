
import ioClient from 'socket.io-client'
const Util = {
  socket: null,

  init (_this) {
    /**
     * Here we connect to our server and pass our headers. eg our authorization heade.
     * The value is taken our from our localStorage.
     */
    this.socket = ioClient.connect('http://localhost:5000', {
      transportOptions: {
        polling: {
          extraHeaders: {
            Authorization: localStorage.getItem('authorization')
          }
        }
      }
    })
    /**
     * In case that our token expiers or is no longer valid.
     * we remove our localstorage item and also reload the document.
     */
    this.socket.on('resetConnection', () => {
      localStorage.removeItem('authorization')
      document.location.reload()
    })

    /**
     * Gets our inital rooms and sets the chats room to them.
     */
    this.socket.on('init', (rooms) => {
      _this.setState({
        rooms: rooms.length ? rooms : [rooms]
      })
    })

    this.socket.emit('init') //To get our init values.

    /**
     * Whenever a user connects the currentlyOnline list gets updates.
     * We search for the correct room and updates its online list.
     */
    this.socket.on('currentlyOnline', (data) => {
      const rooms = _this.state.rooms
      for (let i = 0; i < rooms.length; i++) {
        if (rooms[i].name === data.name) {
          rooms[i].online = data.online
          break
        }
      }
      _this.setState({
        rooms: rooms
      })
    })
    /**
     * Whenever we join a room we get the rooms data return to us.
     */
    this.socket.on('joinedRoom', (room) => {
      const rooms = _this.state.rooms
      rooms.push(room)
      _this.setState({
        rooms: rooms
      })
    })
    /**
     * When we recieve a message we extract different things.
     * We check if a giphy is inside it. eg any word with [] around it.
     * We check if it is a private message. eg containts /message @username@
     * Then we simply send back the information to everyone in th egiven room.
     * If the room isn't the active room we add unread messages to it.
     */
    this.socket.on('message', (data) => {
      const rooms = _this.state.rooms
      rooms.forEach((room, index) => {
        if (room.name === data.room) {
          if (_this.state.currentRoom !== index) {
            room.unread ? room.unread++ : room.unread = 1
            if (data.privateMsg) {
              alert('You got aprivate message from ' + data.username.split('@')[0] + ' in room ' + data.room)
            }
          }
          room.messages.push({ message: data.message, username: data.username, avatar: data.pic, privateMsg: data.privateMsg, date: data.date, color: data.color, giphy: data.giphy })
        }
      })
      _this.setState({
        rooms: rooms
      })
    })
    /**
     * Present a non fatal error.
     */
    this.socket.on('nonFatalError', (error) => {
      alert(error)
    })
    /**
     * Messages such as sucessfully created a room
     */
    this.socket.on('alertMessage', (message) => {
      alert(message)
    })
  },
  /**
   * If we don't have a color set we use black.
   * @param {*} data 
   */
  messageFunc (data) {
    data.color = localStorage.getItem('text') || '#000000'
    this.socket.emit('message', data)
  },
  createRoom (data) {
    this.socket.emit('createRoom', data)
  },
  joinRoom (data) {
    this.socket.emit('joinRoom', data)
  }
}

export default Util
