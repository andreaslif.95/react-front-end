import React from 'react'
import RoomsContainer from '../../components/roomsContainer'
import './index.css'
import ChatContainer from '../../components/chatContainer'
import SocketHandler from './util'

export default class Chat extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      rooms: [],
      currentRoom: 0
    }
  }
  //We init our socketHandler
  componentDidMount () {
    SocketHandler.init(this)
  }

  //When we click on a room we want all the unread message to reset.
  resetUnread (index) {
    const rooms = this.state.rooms
    rooms[index].unread = false
    console.log(rooms)
    this.setState({
      rooms: rooms
    })
  }

  messageFunc (message) {
    SocketHandler.messageFunc({ message, room: this.state.rooms[this.state.currentRoom].name })
  }

  createRoom (name, key) {
    SocketHandler.createRoom({ name, key })
  }

  joinRoom (name, key) {
    SocketHandler.joinRoom({ name, key })
  }

  render () {
    if (this.state.rooms.length > 0) {
      return (
        <div className='appContainer'>
          <RoomsContainer rooms={this.state.rooms} current={this.state.currentRoom} setCurrent={(newRoom) => this.setState({ currentRoom: newRoom })} createRoom={(name, key) => this.createRoom(name, key)} joinRoom={(name, key) => this.joinRoom(name, key)} reset={(index) => this.resetUnread(index)} />
          <ChatContainer messages={this.state.rooms[this.state.currentRoom].messages} online={this.state.rooms[this.state.currentRoom].online} messageFunc={(message) => this.messageFunc(message)} />
        </div>
      )
    } else {
      return (<p>waiting for server..</p>)
    }
  }
}
