import React from 'react'
import './index.css'
import { GoogleLogin } from 'react-google-login'

export default class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = { error: false }

    //We check if we have a token stored, then we try toverify it against the server.
    //If we are authenticated, eg. the server validates us, we set the Auth to true which changes components to Chat
    if (localStorage.getItem('authorization') !== null) {
      fetch('http://localhost:5000/verify', {
        method: 'POST',
        headers: {
          'content-type': 'application/json'
        },
        body: JSON.stringify({
          token: localStorage.getItem('authorization')
        })
      })
      .then((resp) => {
        if (resp.status === 200) {
          props.setAuth(true)
        }
      })
      .catch(err => console.log(err.message))
    }
  }
  /**
   * If we have a sucessfull verification we set email and authorization tokens in our local storage.
   * We also set the state auth in App to true which changes the component to chat.
   * @param {*} res 
   */
  onSuccess (res) {
    localStorage.setItem('authorization', res.tokenId)
    localStorage.setItem('email', res.profileObj.email)
    this.props.setAuth()
  }
  // On error we present an error.
  onFailure (res) {
    this.setState({
      error: true
    })
  }
  /**
   * Uses googleLogin library where we only have to provide our clientId.
   */
  render () {
    return (
      <div className='login'>
        <div className='box'>
          {this.state.error && <p style={{ color: 'red' }}>Failed to auth</p>}
          <GoogleLogin
            clientId='991271249488-9sfaf70fsa4oanjmr41ed0j9b5kgdphm.apps.googleusercontent.com'
            buttonText='LoginWithGoogle'
            onSuccess={(res) => this.onSuccess(res)}
            onFailure={(res) => this.onFailure(res)}
          />
        </div>
      </div>
    )
  }
}
