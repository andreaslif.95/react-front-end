import React, { useState } from 'react'
import './App.css'
import Chat from './pages/chat'
import Login from './pages/login'
/**
 * Conditional rendering. The login component will see if we are authenticated and set this components auth state to true if so.
 */
function App () {
  const [auth, setAuth] = useState(false)
  if (auth) {
    return (
      <Chat />
    )
  } else {
    return (
      <Login setAuth={() => setAuth(true)} />
    )
  }
}

export default App
