import React from 'react'
import MessageListItem from '../messageListItem'
import './index.css'
/**
 * Rendering the message list using the message list item
 * Extracts information from each message in our Messages prop.
 * Scrollsto the bottom on each update.
 * @messages - Messages to be rendered.
 */
export default class MessageList extends React.Component {
  constructor (props) {
    super(props)
    this.ref = React.createRef()
  }

  componentDidUpdate () {
    this.ref.current.scrollTo(0, this.ref.current.scrollHeight)
  }

  render () {
    return (
      <div className='messageList' ref={this.ref}>
        {this.props.messages.map(message =>
          <MessageListItem key={message.index} message={message.message} avatarSrc={message.avatar} username={message.username} privateMsg={message.privateMsg} date={message.date} color={message.color} giphy={message.giphy} />
        )}
      </div>
    )
  }
}
