import React from 'react'
import './index.css'
import RoomList from '../roomList'
export default function RoomsContainer ({ rooms, current, setCurrent, createRoom, joinRoom, reset }) {
  return (
    <div className='roomContainer'>
      <RoomList rooms={rooms} current={current} setCurrent={setCurrent} createRoom={createRoom} joinRoom={joinRoom} reset={reset} />
    </div>
  )
}
