import React from 'react'
import './index.css'
import Avatar from '../avatar'
/**
 * Shows the amount of people online, their avatr and the the name.
 * @param {*} param0 
 */
export default function OnlineList ({ online }) {
  online = online || []
  return (
    <div className='onlineList'>
      <p> -   {online.length} Currently online</p>
      <AvatarList online={online} />
    </div>
  )
}

function AvatarList ({ online }) {
  const cOnline = online.map((user) =>
    <AvatarWrapper key={user.index} username={user.username} src={user.pic} />
  )
  return (
    <div className='avatarList'>
      {cOnline}
    </div>
  )
}

function AvatarWrapper ({ username, src }) {
  return (
    <div className='avatarWrapper'>
      <Avatar src={src} username={username} />
    </div>
  )
}
