import React, { useState } from 'react'
import './index.css'

import RoomListItem from '../roomListItem'
/**
 * Presents the room list which is on the left of the page.
 * Here we have functionality to create/join rooms and set colors.
 * 
 * @rooms -> all the rooms the user has joined
 * @current -> The index of the current room
 * @setCurrent -> function to set the current room
 * @createRoom -> function to create a room
 * @joinRoom -> Functino to join a room
 * @reset -> resets unread messages of specific room.
 * @param {*} param0 
 */
export default function RoomList ({ rooms, current, setCurrent, createRoom, joinRoom, reset }) {
  const roomItems = rooms.map((room, index) =>
    <li
      key={room.name} onClick={() => {
        setCurrent(index)
        reset(index)
      }}
    >
      <RoomListItem name={room.name} active={index === current} messageCount={room.unread} />
    </li>
  )
  const [name, setName] = useState('')
  const [key, setKey] = useState('')
  const [color, setColor] = useState(localStorage.getItem('text') || '#000000')

  const onCreateRoom = () => {
    createRoom(name, key)
    setKey('')
    setName('')
  }

  const onJoinRoom = () => {
    joinRoom(name, key)
    setKey('')
    setName('')
  }
  const colorSet = (e) => {
    localStorage.setItem('text', e.target.value)
    setColor(e.target.value)
  }
  return (
    <ul>
      <li>Join/Create room</li>
      <li><input type='text' placeholder='name' value={name} onChange={(e) => setName(e.target.value)} /></li>
      <li><input type='password' placeholder='password/blank' value={key} onChange={(e) => setKey(e.target.value)} /></li>
      <li><button onClick={onJoinRoom}>Join</button></li>
      <li><button onClick={onCreateRoom}>Create</button></li>
      <li>-- textColor</li>
      <li><input type='color' placeholder='textColor inHex' value={color} onChange={(e) => colorSet(e)} /></li>
      <li>-- Current rooms</li>
      {roomItems}
    </ul>
  )
}
