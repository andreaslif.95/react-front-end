import React from 'react'
import Avatar from '../avatar'
import MessageBubble from '../messagebubble'
import './index.css'
/**
 * A message.
 * Conditional rendering
 *  If server -> we present only the message bubble and position the message in the middle
 *  If -> Sender is the logged in user, we then present only the avatar and message, also we set the avatar to the right instead of the left.
 *  Else present normally.
 */
export default function MessageListItem ({ message, avatarSrc, username, date, privateMsg, color, giphy }) {
  const d = new Date(date)
  if (username === 'SERVER') {
    return (
      <div className='server'>
        <MessageBubble message={message} privateMsg={privateMsg} />
      </div>
    )
  } else if (username !== localStorage.getItem('email')) {
    return (
      <div className='message'>
        <Avatar src={avatarSrc} username={username} />
        <MessageBubble message={message} privateMsg={privateMsg} color={color} giphy={giphy} />
        <span className='date'>{d.toLocaleDateString() + ' ' + d.getHours() + ':' + d.getMinutes()}</span>
      </div>
    )
  } else {
    return (
      <div className='message'>
        <span className='date' />
        <MessageBubble message={message} self color={color} giphy={giphy} />
        <Avatar src={avatarSrc} username={username} />
      </div>
    )
  }
}
