import React from 'react'
import './index.css'
import Chat from '../chat'
import OnlineList from '../onlinelist'
/***
 * Complete char container, has the online lsit and chat.
 * @messages -> all the message for the current chat
 * @online -> everyone online at the current chat
 * @messageFunc -> functino to be called when a new message is written.
 */
export default function ChatContainer ({ messages, online, messageFunc }) {
  return (
    <div className='chatContainer'>
      <Chat messages={messages} messageFunc={messageFunc} />
      <OnlineList online={online} />
    </div>
  )
}
