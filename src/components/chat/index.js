import React from 'react'
import MessageList from '../messageList'
import './index.css'
import MessageInput from '../messageInput'
/**
 * Container for messagelist and input. eg. the chat window.
 * @messages -> all the message for the current chat
 * @messageFunc -> functino to be called when a new message is written.
 */
export default function Chat ({ messages, messageFunc }) {
  return (
    <div className='chat'>
      <MessageList messages={messages} />
      <MessageInput messageFunc={messageFunc} />
    </div>
  )
}
