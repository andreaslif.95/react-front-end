import React from 'react'
import './index.css'
/**
 * Present a avatar and the username of the user.
 * @param {} props - Username and image src
 */
export default function Avatar ({ src, username }) {
  username = username.split('@')[0]
  return (
    <div className='username'>
      <img src={src} />
      <span style={{ textAlign: 'center' }}>{username}</span>
    </div>
  )
}
