import React from 'react'
import './index.css'

/**
 * The ubble holding the message. 
 * Conditional rendering.
 * Will render PÅRIVATE if a private message.
 * Will add the class self if the message is from the current user.
 * Will render a iframe containing the giphy if the message contains a giphy.
 */
export default function MessageBubble ({ message, self, privateMsg, color, giphy }) {
  return (
    <div className={'textBubble' + (self ? ' self' : '')}>
      <span className='textMessage' style={{color: color || '#000000' }}>
        {privateMsg && <b>[PRIVATE]</b>}
        {message}
        {giphy && <iframe src={giphy} width="480" height="320" frameBorder="0" class="giphy-embed" allowFullScreen></iframe>}
      </span>
    </div>
  )
}
