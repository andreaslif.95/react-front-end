import React from 'react'
import './index.css'
/**
 * Handles keyUp and keyDown so that we send the message each time we hit enter.
 * IF and only IF shift isn't clicked on the same time.
 * @messageFunc - Function to be called when a message should be sent.
 */
export default function MessageInput ({ messageFunc }) {
  const keys = []
  const onKeyUpHandle = (event) => {
    keys[event.keyCode] = false
  }
  const onKeyDownHandle = (event) => {
    if (event.keyCode === 13 && !keys[16]) {
      keys[16] = false
      keys[13] = false
      messageFunc(event.target.value)
      event.target.value = ''
    } else {
      keys[event.keyCode] = event.keyCode
    }
  }
  return (
    <textarea className='input' type='text' placeholder='Message' onKeyDown={onKeyDownHandle} onKeyUp={onKeyUpHandle} />

  )
}
