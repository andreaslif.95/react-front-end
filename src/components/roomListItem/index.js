import React from 'react'
import './index.css'

export default function RoomListItem ({ name, active, messageCount }) {
  return (
    <div className={'roomEle' + (active ? ' active' : '')}><span>{name}{!active && messageCount ? ('(' + messageCount + ')') : ''}</span></div>
  )
}
