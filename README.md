## How to use
    Download the server from 
    https://gitlab.com/andreaslif.95/express-back-end
    user node server.js

    Then use npm install in the react folder.
    Login with google.
## Functionality.
    * Join/create room on the left panel.
    * Google authentication.
    * to use giphy send a message like this => '[@term]' where you replace '@term' with the giphy you wanna use.
    * To send a private message you type /message @username@ where you replace only the 'username' part with the person you wanna message.
    * Pick a color from the left panel.
    * See everyone online on the right panel.
